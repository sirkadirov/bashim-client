﻿namespace BashIM
{
    partial class quotePanel
    {
        /// <summary> 
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором компонентов

        /// <summary> 
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(quotePanel));
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.dlikeBtn = new Telerik.WinControls.UI.RadButton();
            this.radButton2 = new Telerik.WinControls.UI.RadButton();
            this.likeBtn = new Telerik.WinControls.UI.RadButton();
            this.visualStudio2012LightTheme1 = new Telerik.WinControls.Themes.VisualStudio2012LightTheme();
            this.radThemeManager1 = new Telerik.WinControls.RadThemeManager();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.radRichTextEditor1 = new Telerik.WinControls.UI.RadRichTextEditor();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dlikeBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.likeBtn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRichTextEditor1)).BeginInit();
            this.SuspendLayout();
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.dlikeBtn);
            this.radPanel1.Controls.Add(this.radButton2);
            this.radPanel1.Controls.Add(this.likeBtn);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.radPanel1.Location = new System.Drawing.Point(0, 175);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(513, 27);
            this.radPanel1.TabIndex = 1;
            this.radPanel1.ThemeName = "VisualStudio2012Light";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radPanel1.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(218)))), ((int)(((byte)(218)))), ((int)(((byte)(218)))));
            // 
            // dlikeBtn
            // 
            this.dlikeBtn.Dock = System.Windows.Forms.DockStyle.Right;
            this.dlikeBtn.Font = new System.Drawing.Font("Wingdings", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.dlikeBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.dlikeBtn.Location = new System.Drawing.Point(388, 0);
            this.dlikeBtn.Name = "dlikeBtn";
            this.dlikeBtn.Size = new System.Drawing.Size(27, 27);
            this.dlikeBtn.TabIndex = 2;
            this.dlikeBtn.Text = "û";
            this.dlikeBtn.ThemeName = "VisualStudio2012Light";
            this.dlikeBtn.Click += new System.EventHandler(this.dlikeBtn_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.dlikeBtn.GetChildAt(0))).Text = "û";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dlikeBtn.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(206)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.dlikeBtn.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(172)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.dlikeBtn.GetChildAt(0).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.dlikeBtn.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // radButton2
            // 
            this.radButton2.Dock = System.Windows.Forms.DockStyle.Right;
            this.radButton2.Font = new System.Drawing.Font("Segoe UI Semibold", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.radButton2.Location = new System.Drawing.Point(415, 0);
            this.radButton2.Name = "radButton2";
            this.radButton2.Size = new System.Drawing.Size(71, 27);
            this.radButton2.TabIndex = 1;
            this.radButton2.Text = "39276";
            this.radButton2.ThemeName = "VisualStudio2012Light";
            ((Telerik.WinControls.UI.RadButtonElement)(this.radButton2.GetChildAt(0))).Text = "39276";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(237)))), ((int)(((byte)(237)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.radButton2.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            // 
            // likeBtn
            // 
            this.likeBtn.Dock = System.Windows.Forms.DockStyle.Right;
            this.likeBtn.Font = new System.Drawing.Font("Wingdings", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.likeBtn.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            this.likeBtn.Location = new System.Drawing.Point(486, 0);
            this.likeBtn.Name = "likeBtn";
            this.likeBtn.Size = new System.Drawing.Size(27, 27);
            this.likeBtn.TabIndex = 0;
            this.likeBtn.Text = "ü";
            this.likeBtn.ThemeName = "VisualStudio2012Light";
            this.likeBtn.Click += new System.EventHandler(this.likeBtn_Click);
            ((Telerik.WinControls.UI.RadButtonElement)(this.likeBtn.GetChildAt(0))).Text = "ü";
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.likeBtn.GetChildAt(0).GetChildAt(0))).BackColor3 = System.Drawing.Color.FromArgb(((int)(((byte)(49)))), ((int)(((byte)(191)))), ((int)(((byte)(0)))));
            ((Telerik.WinControls.Primitives.FillPrimitive)(this.likeBtn.GetChildAt(0).GetChildAt(0))).BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(42)))), ((int)(((byte)(165)))), ((int)(((byte)(0)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.likeBtn.GetChildAt(0).GetChildAt(1).GetChildAt(1))).ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
            ((Telerik.WinControls.Primitives.TextPrimitive)(this.likeBtn.GetChildAt(0).GetChildAt(1).GetChildAt(1))).Alignment = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox1
            // 
            this.pictureBox1.BackgroundImage = global::BashIM.Properties.Resources.star_wars_1_vader_2400x1200_660808050818;
            this.pictureBox1.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.pictureBox1.Dock = System.Windows.Forms.DockStyle.Left;
            this.pictureBox1.Location = new System.Drawing.Point(0, 0);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(170, 175);
            this.pictureBox1.TabIndex = 2;
            this.pictureBox1.TabStop = false;
            // 
            // radRichTextEditor1
            // 
            this.radRichTextEditor1.BorderColor = System.Drawing.Color.Transparent;
            this.radRichTextEditor1.CaretWidth = float.NaN;
            this.radRichTextEditor1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radRichTextEditor1.IsReadOnly = true;
            this.radRichTextEditor1.Location = new System.Drawing.Point(170, 0);
            this.radRichTextEditor1.Name = "radRichTextEditor1";
            this.radRichTextEditor1.SelectionFill = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(93)))), ((int)(((byte)(174)))), ((int)(((byte)(255)))));
            this.radRichTextEditor1.SelectionStroke = System.Drawing.Color.Transparent;
            this.radRichTextEditor1.Size = new System.Drawing.Size(343, 175);
            this.radRichTextEditor1.TabIndex = 3;
            this.radRichTextEditor1.Text = resources.GetString("radRichTextEditor1.Text");
            this.radRichTextEditor1.ThemeName = "VisualStudio2012Light";
            // 
            // quotePanel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.radRichTextEditor1);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.radPanel1);
            this.Name = "quotePanel";
            this.Size = new System.Drawing.Size(513, 202);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dlikeBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radButton2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.likeBtn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.radRichTextEditor1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.Themes.VisualStudio2012LightTheme visualStudio2012LightTheme1;
        private Telerik.WinControls.RadThemeManager radThemeManager1;
        private Telerik.WinControls.UI.RadButton likeBtn;
        private System.Windows.Forms.PictureBox pictureBox1;
        private Telerik.WinControls.UI.RadButton dlikeBtn;
        private Telerik.WinControls.UI.RadButton radButton2;
        private Telerik.WinControls.UI.RadRichTextEditor radRichTextEditor1;
    }
}
