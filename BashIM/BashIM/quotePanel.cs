﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Telerik.WinControls;

namespace BashIM
{
    public partial class quotePanel : UserControl
    {
        public quotePanel()
        {
            InitializeComponent();
        }

        private void dlikeBtn_Click(object sender, EventArgs e)
        {

            //Disable all btns
            likeBtn.Enabled = false;
            dlikeBtn.Enabled = false;
        }

        private void likeBtn_Click(object sender, EventArgs e)
        {

            //Disable all btns
            likeBtn.Enabled = false;
            dlikeBtn.Enabled = false;
        }
    }
}
