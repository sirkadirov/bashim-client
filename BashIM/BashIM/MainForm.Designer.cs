﻿namespace BashIM
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.office2013DarkTheme1 = new Telerik.WinControls.Themes.Office2013DarkTheme();
            this.radPageView1 = new Telerik.WinControls.UI.RadPageView();
            this.radPageViewPage1 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPanel2 = new Telerik.WinControls.UI.RadPanel();
            this.radScrollablePanel1 = new Telerik.WinControls.UI.RadScrollablePanel();
            this.radPanel1 = new Telerik.WinControls.UI.RadPanel();
            this.radPanorama2 = new Telerik.WinControls.UI.RadPanorama();
            this.newQuotesBtn = new Telerik.WinControls.UI.RadTileElement();
            this.bestQuotesBtn = new Telerik.WinControls.UI.RadTileElement();
            this.ratingQuotesBtn = new Telerik.WinControls.UI.RadTileElement();
            this.randomQuotesBtn = new Telerik.WinControls.UI.RadTileElement();
            this.radPageViewPage2 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageViewPage3 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageViewPage5 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPageViewPage4 = new Telerik.WinControls.UI.RadPageViewPage();
            this.radPanorama1 = new Telerik.WinControls.UI.RadPanorama();
            this.radTileElement2 = new Telerik.WinControls.UI.RadTileElement();
            this.radTileElement3 = new Telerik.WinControls.UI.RadTileElement();
            this.radTileElement4 = new Telerik.WinControls.UI.RadTileElement();
            this.radTileElement1 = new Telerik.WinControls.UI.RadTileElement();
            this.radTileElement5 = new Telerik.WinControls.UI.RadTileElement();
            this.radTileElement6 = new Telerik.WinControls.UI.RadTileElement();
            this.radTileElement7 = new Telerik.WinControls.UI.RadTileElement();
            this.radTileElement8 = new Telerik.WinControls.UI.RadTileElement();
            this.radTileElement13 = new Telerik.WinControls.UI.RadTileElement();
            this.radTileElement14 = new Telerik.WinControls.UI.RadTileElement();
            this.radThemeManager1 = new Telerik.WinControls.RadThemeManager();
            this.telerikMetroTouchTheme1 = new Telerik.WinControls.Themes.TelerikMetroTouchTheme();
            this.telerikMetroTheme1 = new Telerik.WinControls.Themes.TelerikMetroTheme();
            this.visualStudio2012LightTheme1 = new Telerik.WinControls.Themes.VisualStudio2012LightTheme();
            this.quotePanel4 = new BashIM.quotePanel();
            this.quotePanel3 = new BashIM.quotePanel();
            this.quotePanel2 = new BashIM.quotePanel();
            this.quotePanel1 = new BashIM.quotePanel();
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).BeginInit();
            this.radPageView1.SuspendLayout();
            this.radPageViewPage1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).BeginInit();
            this.radPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).BeginInit();
            this.radScrollablePanel1.PanelContainer.SuspendLayout();
            this.radScrollablePanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).BeginInit();
            this.radPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanorama2)).BeginInit();
            this.radPageViewPage4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.radPanorama1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this)).BeginInit();
            this.SuspendLayout();
            // 
            // radPageView1
            // 
            this.radPageView1.Controls.Add(this.radPageViewPage1);
            this.radPageView1.Controls.Add(this.radPageViewPage2);
            this.radPageView1.Controls.Add(this.radPageViewPage3);
            this.radPageView1.Controls.Add(this.radPageViewPage5);
            this.radPageView1.Controls.Add(this.radPageViewPage4);
            this.radPageView1.DefaultPage = this.radPageViewPage1;
            this.radPageView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPageView1.Location = new System.Drawing.Point(0, 0);
            this.radPageView1.Name = "radPageView1";
            this.radPageView1.SelectedPage = this.radPageViewPage1;
            this.radPageView1.Size = new System.Drawing.Size(1118, 665);
            this.radPageView1.TabIndex = 0;
            this.radPageView1.ThemeName = "VisualStudio2012Light";
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).ShowItemPinButton = false;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).StripButtons = Telerik.WinControls.UI.StripViewButtons.None;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).ItemAlignment = Telerik.WinControls.UI.StripViewItemAlignment.Center;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).StripAlignment = Telerik.WinControls.UI.StripViewAlignment.Top;
            ((Telerik.WinControls.UI.RadPageViewStripElement)(this.radPageView1.GetChildAt(0))).ShowItemCloseButton = false;
            // 
            // radPageViewPage1
            // 
            this.radPageViewPage1.Controls.Add(this.radPanel2);
            this.radPageViewPage1.Controls.Add(this.radPanel1);
            this.radPageViewPage1.ItemSize = new System.Drawing.SizeF(50F, 24F);
            this.radPageViewPage1.Location = new System.Drawing.Point(5, 30);
            this.radPageViewPage1.Name = "radPageViewPage1";
            this.radPageViewPage1.Size = new System.Drawing.Size(1108, 630);
            this.radPageViewPage1.Text = "Цитаты";
            // 
            // radPanel2
            // 
            this.radPanel2.Controls.Add(this.radScrollablePanel1);
            this.radPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanel2.Location = new System.Drawing.Point(227, 0);
            this.radPanel2.Name = "radPanel2";
            this.radPanel2.Size = new System.Drawing.Size(881, 630);
            this.radPanel2.TabIndex = 1;
            this.radPanel2.ThemeName = "VisualStudio2012Light";
            // 
            // radScrollablePanel1
            // 
            this.radScrollablePanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radScrollablePanel1.Location = new System.Drawing.Point(0, 0);
            this.radScrollablePanel1.Name = "radScrollablePanel1";
            // 
            // radScrollablePanel1.PanelContainer
            // 
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.quotePanel4);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.quotePanel3);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.quotePanel2);
            this.radScrollablePanel1.PanelContainer.Controls.Add(this.quotePanel1);
            this.radScrollablePanel1.PanelContainer.Size = new System.Drawing.Size(862, 628);
            this.radScrollablePanel1.Size = new System.Drawing.Size(881, 630);
            this.radScrollablePanel1.TabIndex = 0;
            this.radScrollablePanel1.Text = "radScrollablePanel1";
            this.radScrollablePanel1.ThemeName = "VisualStudio2012Light";
            // 
            // radPanel1
            // 
            this.radPanel1.Controls.Add(this.radPanorama2);
            this.radPanel1.Dock = System.Windows.Forms.DockStyle.Left;
            this.radPanel1.Location = new System.Drawing.Point(0, 0);
            this.radPanel1.Name = "radPanel1";
            this.radPanel1.Size = new System.Drawing.Size(227, 630);
            this.radPanel1.TabIndex = 0;
            this.radPanel1.ThemeName = "VisualStudio2012Light";
            // 
            // radPanorama2
            // 
            this.radPanorama2.AllowDragDrop = false;
            this.radPanorama2.AutoScroll = true;
            this.radPanorama2.CellSize = new System.Drawing.Size(210, 100);
            this.radPanorama2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanorama2.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.newQuotesBtn,
            this.bestQuotesBtn,
            this.ratingQuotesBtn,
            this.randomQuotesBtn});
            this.radPanorama2.Location = new System.Drawing.Point(0, 0);
            this.radPanorama2.Name = "radPanorama2";
            this.radPanorama2.RowsCount = 10;
            this.radPanorama2.Size = new System.Drawing.Size(227, 630);
            this.radPanorama2.TabIndex = 0;
            this.radPanorama2.Text = "radPanorama2";
            this.radPanorama2.ThemeName = "VisualStudio2012Light";
            this.radPanorama2.Click += new System.EventHandler(this.radPanorama2_Click);
            // 
            // newQuotesBtn
            // 
            this.newQuotesBtn.Name = "newQuotesBtn";
            this.newQuotesBtn.Text = "Новые";
            // 
            // bestQuotesBtn
            // 
            this.bestQuotesBtn.Name = "bestQuotesBtn";
            this.bestQuotesBtn.Row = 1;
            this.bestQuotesBtn.Text = "Лучшие";
            // 
            // ratingQuotesBtn
            // 
            this.ratingQuotesBtn.Name = "ratingQuotesBtn";
            this.ratingQuotesBtn.Row = 2;
            this.ratingQuotesBtn.Text = "По рейтингу";
            // 
            // randomQuotesBtn
            // 
            this.randomQuotesBtn.Name = "randomQuotesBtn";
            this.randomQuotesBtn.Row = 3;
            this.randomQuotesBtn.Text = "Случайные";
            // 
            // radPageViewPage2
            // 
            this.radPageViewPage2.ItemSize = new System.Drawing.SizeF(48F, 24F);
            this.radPageViewPage2.Location = new System.Drawing.Point(0, 0);
            this.radPageViewPage2.Name = "radPageViewPage2";
            this.radPageViewPage2.Size = new System.Drawing.Size(200, 100);
            this.radPageViewPage2.Text = "Бездна";
            // 
            // radPageViewPage3
            // 
            this.radPageViewPage3.ItemSize = new System.Drawing.SizeF(62F, 24F);
            this.radPageViewPage3.Location = new System.Drawing.Point(0, 0);
            this.radPageViewPage3.Name = "radPageViewPage3";
            this.radPageViewPage3.Size = new System.Drawing.Size(200, 100);
            this.radPageViewPage3.Text = "Добавить";
            // 
            // radPageViewPage5
            // 
            this.radPageViewPage5.ItemSize = new System.Drawing.SizeF(70F, 24F);
            this.radPageViewPage5.Location = new System.Drawing.Point(0, 0);
            this.radPageViewPage5.Name = "radPageViewPage5";
            this.radPageViewPage5.Size = new System.Drawing.Size(200, 100);
            this.radPageViewPage5.Text = "Избранное";
            // 
            // radPageViewPage4
            // 
            this.radPageViewPage4.Controls.Add(this.radPanorama1);
            this.radPageViewPage4.ItemSize = new System.Drawing.SizeF(22F, 24F);
            this.radPageViewPage4.Location = new System.Drawing.Point(5, 30);
            this.radPageViewPage4.Name = "radPageViewPage4";
            this.radPageViewPage4.Size = new System.Drawing.Size(1108, 630);
            this.radPageViewPage4.Text = "....";
            // 
            // radPanorama1
            // 
            this.radPanorama1.AllowDragDrop = false;
            this.radPanorama1.CellSize = new System.Drawing.Size(200, 100);
            this.radPanorama1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.radPanorama1.Items.AddRange(new Telerik.WinControls.RadItem[] {
            this.radTileElement2,
            this.radTileElement3,
            this.radTileElement4,
            this.radTileElement1,
            this.radTileElement5,
            this.radTileElement6,
            this.radTileElement7,
            this.radTileElement8,
            this.radTileElement13,
            this.radTileElement14});
            this.radPanorama1.Location = new System.Drawing.Point(0, 0);
            this.radPanorama1.Name = "radPanorama1";
            this.radPanorama1.RowsCount = 4;
            this.radPanorama1.Size = new System.Drawing.Size(1108, 630);
            this.radPanorama1.TabIndex = 0;
            this.radPanorama1.Text = "radPanorama1";
            this.radPanorama1.ThemeName = "VisualStudio2012Light";
            this.radPanorama1.Click += new System.EventHandler(this.radPanorama1_Click);
            // 
            // radTileElement2
            // 
            this.radTileElement2.Name = "radTileElement2";
            this.radTileElement2.Text = "О программе";
            // 
            // radTileElement3
            // 
            this.radTileElement3.Name = "radTileElement3";
            this.radTileElement3.Row = 1;
            this.radTileElement3.Text = "Справка";
            // 
            // radTileElement4
            // 
            this.radTileElement4.Name = "radTileElement4";
            this.radTileElement4.Row = 2;
            this.radTileElement4.Text = "Сайт\r\nприложения";
            // 
            // radTileElement1
            // 
            this.radTileElement1.Name = "radTileElement1";
            this.radTileElement1.Row = 3;
            this.radTileElement1.Text = "Об авторе";
            // 
            // radTileElement5
            // 
            this.radTileElement5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.radTileElement5.Column = 1;
            this.radTileElement5.Name = "radTileElement5";
            this.radTileElement5.Text = "BashIM";
            // 
            // radTileElement6
            // 
            this.radTileElement6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.radTileElement6.Column = 1;
            this.radTileElement6.Name = "radTileElement6";
            this.radTileElement6.Row = 1;
            this.radTileElement6.Text = "Обратная связь";
            // 
            // radTileElement7
            // 
            this.radTileElement7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.radTileElement7.Column = 1;
            this.radTileElement7.Name = "radTileElement7";
            this.radTileElement7.Row = 2;
            this.radTileElement7.Text = "Лицензионное\r\nсоглашение";
            // 
            // radTileElement8
            // 
            this.radTileElement8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(204)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.radTileElement8.Column = 1;
            this.radTileElement8.Name = "radTileElement8";
            this.radTileElement8.Row = 3;
            this.radTileElement8.Text = "FAQ";
            // 
            // radTileElement13
            // 
            this.radTileElement13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(181)))), ((int)(((byte)(0)))));
            this.radTileElement13.Column = 2;
            this.radTileElement13.Name = "radTileElement13";
            this.radTileElement13.Text = "Настройки";
            // 
            // radTileElement14
            // 
            this.radTileElement14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(38)))), ((int)(((byte)(181)))), ((int)(((byte)(0)))));
            this.radTileElement14.Column = 2;
            this.radTileElement14.Name = "radTileElement14";
            this.radTileElement14.Row = 1;
            this.radTileElement14.Text = "Стиль\r\nприложения";
            // 
            // quotePanel4
            // 
            this.quotePanel4.Dock = System.Windows.Forms.DockStyle.Top;
            this.quotePanel4.Location = new System.Drawing.Point(0, 606);
            this.quotePanel4.Name = "quotePanel4";
            this.quotePanel4.Size = new System.Drawing.Size(862, 202);
            this.quotePanel4.TabIndex = 3;
            // 
            // quotePanel3
            // 
            this.quotePanel3.Dock = System.Windows.Forms.DockStyle.Top;
            this.quotePanel3.Location = new System.Drawing.Point(0, 404);
            this.quotePanel3.Name = "quotePanel3";
            this.quotePanel3.Size = new System.Drawing.Size(862, 202);
            this.quotePanel3.TabIndex = 2;
            // 
            // quotePanel2
            // 
            this.quotePanel2.Dock = System.Windows.Forms.DockStyle.Top;
            this.quotePanel2.Location = new System.Drawing.Point(0, 202);
            this.quotePanel2.Name = "quotePanel2";
            this.quotePanel2.Size = new System.Drawing.Size(862, 202);
            this.quotePanel2.TabIndex = 1;
            // 
            // quotePanel1
            // 
            this.quotePanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.quotePanel1.Location = new System.Drawing.Point(0, 0);
            this.quotePanel1.Name = "quotePanel1";
            this.quotePanel1.Size = new System.Drawing.Size(862, 202);
            this.quotePanel1.TabIndex = 0;
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1118, 665);
            this.Controls.Add(this.radPageView1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "MainForm";
            // 
            // 
            // 
            this.RootElement.ApplyShapeToControl = true;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "BashIM Client";
            this.ThemeName = "VisualStudio2012Light";
            this.Load += new System.EventHandler(this.MainForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.radPageView1)).EndInit();
            this.radPageView1.ResumeLayout(false);
            this.radPageViewPage1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel2)).EndInit();
            this.radPanel2.ResumeLayout(false);
            this.radScrollablePanel1.PanelContainer.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radScrollablePanel1)).EndInit();
            this.radScrollablePanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanel1)).EndInit();
            this.radPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanorama2)).EndInit();
            this.radPageViewPage4.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.radPanorama1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Telerik.WinControls.Themes.Office2013DarkTheme office2013DarkTheme1;
        private Telerik.WinControls.UI.RadPageView radPageView1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage2;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage3;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage4;
        private Telerik.WinControls.RadThemeManager radThemeManager1;
        private Telerik.WinControls.Themes.TelerikMetroTouchTheme telerikMetroTouchTheme1;
        private Telerik.WinControls.Themes.TelerikMetroTheme telerikMetroTheme1;
        private Telerik.WinControls.Themes.VisualStudio2012LightTheme visualStudio2012LightTheme1;
        private Telerik.WinControls.UI.RadPanel radPanel2;
        private Telerik.WinControls.UI.RadPanel radPanel1;
        private Telerik.WinControls.UI.RadPageViewPage radPageViewPage5;
        private Telerik.WinControls.UI.RadPanorama radPanorama1;
        private Telerik.WinControls.UI.RadTileElement radTileElement2;
        private Telerik.WinControls.UI.RadTileElement radTileElement3;
        private Telerik.WinControls.UI.RadTileElement radTileElement4;
        private Telerik.WinControls.UI.RadTileElement radTileElement1;
        private Telerik.WinControls.UI.RadTileElement radTileElement5;
        private Telerik.WinControls.UI.RadTileElement radTileElement6;
        private Telerik.WinControls.UI.RadTileElement radTileElement7;
        private Telerik.WinControls.UI.RadTileElement radTileElement8;
        private Telerik.WinControls.UI.RadPanorama radPanorama2;
        private Telerik.WinControls.UI.RadTileElement newQuotesBtn;
        private Telerik.WinControls.UI.RadTileElement bestQuotesBtn;
        private Telerik.WinControls.UI.RadTileElement ratingQuotesBtn;
        private Telerik.WinControls.UI.RadTileElement randomQuotesBtn;
        private Telerik.WinControls.UI.RadTileElement radTileElement13;
        private Telerik.WinControls.UI.RadTileElement radTileElement14;
        private Telerik.WinControls.UI.RadScrollablePanel radScrollablePanel1;
        private quotePanel quotePanel4;
        private quotePanel quotePanel3;
        private quotePanel quotePanel2;
        private quotePanel quotePanel1;
    }
}